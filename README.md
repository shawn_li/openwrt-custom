#### 使用

一键命令

```yaml
sed -i '$a src-git custom https://gitee.com/shawn_li/openwrt-custom.git' feeds.conf.default
git pull

./scripts/feeds update custom
./scripts/feeds install -a -p custom

make menuconfig
```

- OpenWrt 固件编译自定义主题与软件

| 软件名称                    |   菜单名称    |
|:------------------------|:---------:|
| luci-app-advanced       |   高级设置    |
| luci-app-autotimeset    |   定时设置    |
| luci-app-eqos           |  IP地址限速   |
| luci-app-netspeedtest   |   网速测速    |
| luci-app-oaf            |   应用过滤    |
| luci-app-onliner        |   在线用户    |
| luci-app-openclash      | OpenClash |
| luci-app-parentcontrol  |   家长控制    |
| luci-app-partexp        |   分区扩容    |
| luci-app-store          |   应用商店    |
| luci-app-wizard         |   设置向导    |
| luci-app-wolplus        |   网络唤醒    |
